const http = require('http');
const async = require('async');
var request = require('request');

http.createServer((res, response) => {
    //const Endpoint = "http://nodejs-listener-app-route-node-listener.192.168.64.4.nip.io/api/Catering"
    const Endpoint = "http://node-connect-yili-handy-apps.apps.dev.ldcloud.com.au"
    const { headers, method, url } = res;
    let body = [];
    res.on('error', (err) => {
        //console.error(err);
    }).on('data', (chunk) => {
        body.push(chunk);
    }).on('end', () => {
        body = Buffer.concat(body).toString();
        // BEGINNING OF NEW STUFF

        response.on('error', (err) => {
            //console.error(err);
        });


        const responseBody = { headers, method, url, body };

        
        if (method == 'POST') {
//            console.log(JSON.stringify(body));
            var myBody = (JSON.stringify(body).
            replace(/\\t/g, '').
            replace(/\\n/g, '').
            replace(/\\/g, '')).slice(1, -1);
            myBody = JSON.parse(myBody);
            console.log(myBody);
            var optionsPOST = {
                uri: Endpoint,
                method: 'POST',
                // auth: {
                //   user: 'admin',
                //   pass: 'Yi123456',
                // }, 
                headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
                json: myBody
            };
            request(optionsPOST, function (error, res, body) {
                if (!error && response.statusCode == 200 || response.statusCode == 201 || response.statusCode == 203 || response.statusCode == 204) {
                    //body = JSON.stringify(body);
                        console.log(body)
                        response.write(JSON.stringify(body));
                        response.end();
            
                } else {
                    if (typeof body == 'object') {
                        response.write(JSON.stringify(body));
                        response.end();
                    }else{
                    console.log(error);
                    response.write(body);
                    response.end();
                    }
                }
            });
        } else if (method == 'GET') {
            var optionsGET = {
                uri: Endpoint,
                method: 'GET',
                // auth: {
                //   user: 'admin',
                //   pass: 'Yi123456',
                // }, 
            };
            request(optionsGET, function (error, res, body) {
                if (!error && response.statusCode == 200 || response.statusCode == 201 || response.statusCode == 203 || response.statusCode == 204) {
                    //body = JSON.stringify(body);
                        console.log(body)
                        response.write(JSON.stringify(body));
                        response.end();
            
                } else {
                    if (typeof body == 'object') {
                        response.write(JSON.stringify(body));
                        response.end();
                    }else{
                    console.log(error);
                    response.write(body);
                    response.end();
                    }
                }
            });
        } else {
            response.write("Method not allowed.");
            response.end();
        }
    });


}).listen(8123);

console.log('Server running at ' + 8123);
